export interface Objective {
  id: number,
  title: string,
}

export interface KeyResult {
  name: string,
  percentage: number
}
