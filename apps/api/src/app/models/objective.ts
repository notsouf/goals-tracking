import mongoose from 'mongoose';

const { Schema } = mongoose;

const objectiveSchema  = new Schema({
  title: {
    type: String,
    required: true
  }
});

export default mongoose.model('Objective', objectiveSchema);
