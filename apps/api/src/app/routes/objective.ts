import express from 'express';
import {
  addAnObjective,
  deleteObjective,
  getAllObjectives,
  getAnObjective,
  updateObjective
} from '../controllers/objective';

const router = express.Router();

router.post('/', addAnObjective);

router.get('/', getAllObjectives);

router.get('/:id', getAnObjective);

router.put('/:id', updateObjective);

router.delete('/:id', deleteObjective);


export default router;
