import Objective from '../models/objective';

export const addAnObjective = (req, res, next) => {
  const { title } = req.body;
  const objective = new Objective({ title });
  objective.save()
    .then(() => {
      console.log('Created a new Objective');
      res.json({ message: 'Objective successfully created' });
    }).catch((err) => {
    console.log(err);
    res.status(500).end()
  });
};

export const getAllObjectives = (req, res, next) => {
  Objective.find()
    .then(objectives => res.json(objectives))
    .catch(err => console.log(err));
};

export const getAnObjective = (req, res, next) => {
  const { id } = req.params;
  Objective.findById(id)
    .then(objective => res.json(objective))
    .catch(err => console.log(err));
};

export const updateObjective = (req, res, next) => {
  const { id } = req.params;
  const { title } = req.body;
  Objective.findByIdAndUpdate(id, { title })
    .then(objective => res.json({ message: 'Objective successfully updated' }))
    .catch(err => console.log(err));
};

export const deleteObjective = (req,res, next)=>{
  const { id } = req.params;
  Objective.findByIdAndDelete(id)
    .then(objective => res.json({ message: 'Objective successfully delete' }))
    .catch(err=>console.log(err))
}
