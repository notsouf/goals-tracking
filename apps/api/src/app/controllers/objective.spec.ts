import Objective from '../models/objective';

import { addAnObjective } from './objective';
jest.mock('./objective')
describe('Ojective Controller', () => {
  describe('addAnObjective', () => {
    it('stores an new objective to db if request contains a title', ()=>{
      const req = {
        body: {
          title: 'New oBjective'
        }
      }
      addAnObjective()
      expect().toHaveProperty('title')
    });
  });
});
