import app from "./app";
import mongoose from 'mongoose'

mongoose.connect('mongodb+srv://backend:express@goal-tracking.rbvnl.mongodb.net/objective-app?retryWrites=true&w=majority')
  .then(() => {
    const port = process.env.port || 3333;
    const server = app.listen(port, () => {
      console.log('Listening at http://localhost:' + port + '/api');
    })
    server.on('error', console.error);
  })
  .catch((err) => {
    console.log(err);
  })
