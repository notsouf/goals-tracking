import express from 'express'

import objectiveRoute from './app/routes/objective';
import { addAnObjective } from './app/controllers/objective';

const app = express();

app.use(express.urlencoded())

app.use('/api/objective',objectiveRoute );

export default app
