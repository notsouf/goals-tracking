import request from 'supertest'
import app from './src/main'
import { Response } from 'express'
import { Objective } from '@goal-tracking-app/api-interfaces'

describe('app', () => {
    describe('/api', () => {
        let response
        beforeAll(async () => {
            response = await request(app).get('/api')
        })
        it('responds with 200', async () => {
            expect(response.statusCode).toBe(200)
        })

        it('responds with a json conten-type header', async () => {
            expect(response.header["content-type"]).toContain('application/json')
        })
    })

    describe('/api/goals', ()=> {
     let response
     beforeAll(async () => {
        response = await request(app).get('/api/goals')
    })

    it('responds responds with 200', ()=> {
        expect(response.statusCode).toBe(200)
    })

    it('responds with a list of goals', ()=>{
        const expected : Array<Objective> = [{id:1,name:'Courir le marathon de paris',KeyResults:[{name:'courir 10km', percentage: 0}]}]
        expect(response.text).toBe(expected)
    })
    })


})
