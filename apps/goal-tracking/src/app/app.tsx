import React, { useEffect, useState } from 'react';
import { Objective } from '@goal-tracking-app/api-interfaces';

export const App = () => {
  const [objectives, setObjectives] = useState<Objective[]>([]);

  useEffect(() => {
    fetch('/api/objective')
      .then((r) => r.json())
      .then(setObjectives);
  }, []);

  return (
    <>
      <div style={{ textAlign: 'center' }}>
        <h1 className='title'>Goal Tracking app</h1>
        <h3 className='subtitle'>Gotta track'em all</h3>
        <button className='button is-primary'>Add New Goal</button>
      </div>
      <div>{objectives.map(objective=>{
        return (
          <p>{objective.title}</p>
        )
      })}</div>
    </>
  );
};

export default App;
