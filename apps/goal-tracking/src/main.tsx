import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom';

import App from './app/app';
import 'bulma/css/bulma.min.css'

ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById('root')
);
